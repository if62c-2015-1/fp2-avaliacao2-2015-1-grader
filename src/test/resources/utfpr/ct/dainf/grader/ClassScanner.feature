Feature: Avaliação 2 de IF62C-Fundamentos de Programação 2
    Como professor da disciplica de Fundamentos de Programação 2
    Quero avaliar os módulo 4 e 5
    A fim de verificar a compreensão dos estudantes

    Background:
        Given the maximum grade is 200
        Given the main class is 'Avaliacao2'
        Given I set the script timeout to 3000
        Given I evaluate 'import utfpr.ct.dainf.if62c.avaliacao.*'
        Given I evaluate 'import utfpr.ct.dainf.grader.*'
        Given I evaluate 'import java.util.regex.*'
        # variáveis auxiliares para testar os métodos
        Given I evaluate 'double ZERO = 0.0'
        Given I evaluate 'double x1 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double y1 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double z1 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double x2 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double y2 = TestUtils.random(-3.0, 3.0)'
        Given I evaluate 'double z2 = TestUtils.random(-3.0, 3.0)'
    
    Scenario: Mensagem de inicial.
        Given I report 'Iniciando avaliação...'

    Scenario: Verifica se a classe Ponto está corretamente implementada.
        Given class 'utfpr.ct.dainf.if62c.avaliacao.Ponto' exists store class in <pontoClass>
        
    Scenario: Verifica se a classe Ponto declara o construtor padrão corretamente.
        Given I report 'Avaliando item 1...'
        Given I evaluate 'double ZERO = 0.0; pontoTest = new Ponto();'
        And field 'x' value in <pontoTest> equals <ZERO>
        And field 'y' value in <pontoTest> equals <ZERO>
        And field 'z' value in <pontoTest> equals <ZERO>
        Then award 5 points

    Scenario: Verifica se a classe Ponto declara o construtor Ponto(double,double,double) corretamente.
        Given I report 'Avaliando item 2...'
        Given I evaluate 'pontoTest = new Ponto(x1, y1, z1);'
        And field 'x' value in <pontoTest> equals <x1>
        And field 'y' value in <pontoTest> equals <y1>
        And field 'z' value in <pontoTest> equals <z1>
        Then award 5 points

    Scenario: Verifica se Ponto.toString() esta declarado corretamente.
        Given I report 'Avaliando item 3...'
        Given I evaluate 'pontoTest = new Ponto(x1,y1,z1)'
        Given I evaluate 'pontoStr = pontoTest.toString()'
        Then award 5 points
        Given I evaluate 'regexTest = Aval2Utils.getPontoRegex(x1,y1,z1)'
        And <pontoStr> matches regex <regexTest>
        Then award 5 points

    Scenario: Verifica se Ponto.equals(Object) esta declarado corretamente.
        Given I report 'Avaliando item 4...'
        Given class <pontoClass> declares 'equals(java.lang.Object)' method save in <dummy>
        Given I evaluate 'ponto1 = new Ponto(x1,y1,z1)'
        Given I evaluate 'ponto2 = new Ponto(x1,y1,z1)'
        Given I evaluate 'ponto3 = new Ponto(x2,y2,z2)'
        And expression 'ponto1.equals(ponto2)' evaluates to <true>
        And expression 'ponto1.equals(ponto3)' evaluates to <false>
        Then award 4 points

    Scenario: Verifica se Ponto.equals(Object) trata corretamente o caso de argumento nulo.
        Given expression 'ponto1.equals(null)' evaluates to <false>
        Then award 3 points

    Scenario: Verifica se Ponto.equals(Object) trata corretamente o caso de argumento de tipo diferente.
        Given expression 'ponto1.equals(new String())' evaluates to <false>
        Then award 3 points

    Scenario: Verifica se Ponto.dist(Ponto) esta declarado corretamente.
        Given I report 'Avaliando item 5...'
        Given class <pontoClass> declares 'dist(utfpr.ct.dainf.if62c.avaliacao.Ponto)' method save in <distMethod>
        And method <distMethod> returns type 'double'
        And member <distMethod> has 'public' modifier
        Then award 5 points
        Given I evaluate 'ponto1 = new Ponto(x1,y1,z1)'
        Given I evaluate 'ponto2 = new Ponto(x2,y2,z2)'
        Given I evaluate 'dist = ponto1.dist(ponto2)'
        And expression 'TestUtils.equalsAprox(Aval2Utils.dist(x1,y1,z1,x2,y2,z2),dist,0.001)' evaluates to <true>
        Then award 5 points

    Scenario: Verifica se a classe Ponto2D existe e é pública.
        Given I report 'Avaliando item 6...'
        Given class 'utfpr.ct.dainf.if62c.avaliacao.Ponto2D' exists store class in <ponto2DClass>
        And class <ponto2DClass> has 'public' modifier
        And class 'utfpr.ct.dainf.if62c.avaliacao.Ponto' is assignable from <ponto2DClass> 
        Then award 5 points

    Scenario: Verifica se a classe Ponto2D declara o construtor padrão corretamente.
        Given class <ponto2DClass> declares 'Ponto2D()' constructor save in <ponto2DConstructor> 
        Given member <ponto2DConstructor> has 'protected' modifier
        Then award 5 points

    Scenario: Verifica se a classe Ponto2D declara o construtor Ponto2D(double,double,double) corretamente.
        Given class <ponto2DClass> declares 'Ponto2D(double,double,double)' constructor save in <ponto2DConstructor> 
        Given member <ponto2DConstructor> has 'protected' modifier
        Then award 5 points

    Scenario: Verifica se a classe PontoXY existe é pública e extende Ponto2D.
        Given I report 'Avaliando item 7...'
        Given class 'utfpr.ct.dainf.if62c.avaliacao.PontoXY' exists store class in <ptClass>
        And class <ptClass> has 'public' modifier
        And class 'utfpr.ct.dainf.if62c.avaliacao.Ponto2D' is assignable from <ptClass> 
        Then award 2 points

    Scenario: Verifica se a classe PontoXY declara o construtor padrão corretamente.
        Given class <ptClass> declares 'PontoXY()' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoXY()'
        And expression 'ponto5.getX()' evaluates to <ZERO>
        And expression 'ponto5.getY()' evaluates to <ZERO>
        And expression 'ponto5.getZ()' evaluates to <ZERO>
        Then award 2 points

    Scenario: Verifica se a classe PontoXY declara o construtor PontoXY(double,double) corretamente.
        Given class <ptClass> declares 'PontoXY(double,double)' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoXY(x1,y1)'
        And expression 'ponto5.getX()' evaluates to <x1>
        And expression 'ponto5.getY()' evaluates to <y1>
        And expression 'ponto5.getZ()' evaluates to <ZERO>
        Then award 3 points

    Scenario: Verifica se PontoXY.toString() esta declarado corretamente.
        Given I evaluate 'pontoTest = new PontoXY(x1,y1)'
        Given I evaluate 'pontoStr = pontoTest.toString()'
        Given I evaluate 'regexTest = Aval2Utils.getPonto2DRegex("PontoXY",x1,y1)'
        And <pontoStr> matches regex <regexTest>
        Then award 3 points

    Scenario: Verifica se a classe PontoXZ existe é pública e extende Ponto2D.
        Given class 'utfpr.ct.dainf.if62c.avaliacao.PontoXZ' exists store class in <ptClass>
        And class <ptClass> has 'public' modifier
        And class 'utfpr.ct.dainf.if62c.avaliacao.Ponto2D' is assignable from <ptClass> 
        Then award 2 points

    Scenario: Verifica se a classe PontoXZ declara o construtor padrão corretamente.
        Given class <ptClass> declares 'PontoXZ()' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoXZ()'
        And expression 'ponto5.getX()' evaluates to <ZERO>
        And expression 'ponto5.getY()' evaluates to <ZERO>
        And expression 'ponto5.getZ()' evaluates to <ZERO>
        Then award 2 points

    Scenario: Verifica se a classe PontoXZ declara o construtor PontoXZ(double,double) corretamente.
        Given class <ptClass> declares 'PontoXZ(double,double)' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoXZ(x1,z1)'
        And expression 'ponto5.getX()' evaluates to <x1>
        And expression 'ponto5.getY()' evaluates to <ZERO>
        And expression 'ponto5.getZ()' evaluates to <z1>
        Then award 3 points

    Scenario: Verifica se PontoXZ.toString() esta declarado corretamente.
        Given I evaluate 'pontoTest = new PontoXZ(x1,z1)'
        Given I evaluate 'pontoStr = pontoTest.toString()'
        Given I evaluate 'regexTest = Aval2Utils.getPonto2DRegex("PontoXZ",x1,z1)'
        And <pontoStr> matches regex <regexTest>
        Then award 3 points

    Scenario: Verifica se a classe PontoYZ existe é pública e extende Ponto2D.
        Given class 'utfpr.ct.dainf.if62c.avaliacao.PontoYZ' exists store class in <ptClass>
        And class <ptClass> has 'public' modifier
        And class 'utfpr.ct.dainf.if62c.avaliacao.Ponto2D' is assignable from <ptClass> 
        Then award 2 points

    Scenario: Verifica se a classe PontoYZ declara o construtor padrão corretamente.
        Given class <ptClass> declares 'PontoYZ()' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoYZ()'
        And expression 'ponto5.getX()' evaluates to <ZERO>
        And expression 'ponto5.getY()' evaluates to <ZERO>
        And expression 'ponto5.getZ()' evaluates to <ZERO>
        Then award 2 points

    Scenario: Verifica se a classe PontoYZ declara o construtor PontoYZ(double,double) corretamente.
        Given class <ptClass> declares 'PontoYZ(double,double)' constructor save in <ptConstructor> 
        Given member <ptConstructor> has 'public' modifier
        Given I evaluate 'ponto5 = new PontoYZ(y1,z1)'
        And expression 'ponto5.getX()' evaluates to <ZERO>
        And expression 'ponto5.getY()' evaluates to <y1>
        And expression 'ponto5.getZ()' evaluates to <z1>
        Then award 3 points

    Scenario: Verifica se PontoYZ.toString() esta declarado corretamente.
        Given I evaluate 'pontoTest = new PontoYZ(y1,z1)'
        Given I evaluate 'pontoStr = pontoTest.toString()'
        Given I evaluate 'regexTest = Aval2Utils.getPonto2DRegex("PontoYZ",y1,z1)'
        And <pontoStr> matches regex <regexTest>
        Then award 3 points

    Scenario: Verifica se a classe Poligonal existe e é pública e declara o campo vertices.
        Given I report 'Avaliando item 8...'
        Given class 'utfpr.ct.dainf.if62c.avaliacao.Poligonal' exists store class in <poliClass>
        And class <poliClass> has 'public' modifier
        Given class <poliClass> declares field 'vertices' save in <vertField>
        And field <vertField> is of type '[Lutfpr.ct.dainf.if62c.avaliacao.Ponto2D;'
        And member <vertField> has 'private' modifier
        Then award 5 points

    Scenario: Verifica se a classe Poligonal declara o construtor Poligonal(int) corretamente.
        Given class <poliClass> declares 'Poligonal(int)' constructor save in <poliConstructor> 
        Given member <poliConstructor> has 'public' modifier
        Given I evaluate 'plen = TestUtils.random(3,10)'
        Given I evaluate 'poli = new Poligonal(plen)'
        And I get field 'vertices' value in super class of <poli> save in <pvert>
        And expression 'pvert.length' evaluates to <plen>
        Then award 5 points

    Scenario: Verifica se a Poligonal(int) testa 0 < n < 2.
        Given evaluating 'new Poligonal(1)' throws instance of 'java.lang.RuntimeException' save in <pexcept>
        And I evaluate 'emsg = pexcept.getMessage()'
        And <emsg> matches regex '^\s*[Pp]oligonal.+$' 
        Then award 5 points

    Scenario: Verifica se a Poligonal(int) testa n < 0.
        Given evaluating 'new Poligonal(-1)' throws instance of 'java.lang.RuntimeException' save in <pexcept>
        And I evaluate 'emsg = pexcept.getMessage()'
        And <emsg> matches regex '^\s*[Pp]oligonal.+$' 
        Then award 5 points

    Scenario: Verifica se Poligonal.getN() esta declarado corretamente.
        Given I report 'Avaliando item 9...'
        Given I evaluate 'plen = TestUtils.random(3,10)'
        Given I evaluate 'poli = new Poligonal(plen)'
        And expression 'poli.getN()' evaluates to <plen>
        Then award 5 points

    Scenario: Verifica se Poligonal.get(int) esta declarado corretamente.
        Given I report 'Avaliando item 10...'
        Given I evaluate 'plen = TestUtils.random(3,10); iv = plen/2'
        Given I evaluate 'poli = new Poligonal(plen)'
        Given I get field 'vertices' value in super class of <poli> save in <pvert>
        Given I evaluate 'ptest = new PontoXZ(x1,z1)'
        Given I evaluate 'pvert[iv] = ptest'
        And expression 'poli.get(iv) == ptest' evaluates to <true>
        Then award 5 points

    Scenario: Verifica se Poligonal.get(int) trata corretamente o caso i < 0 ou i >= n.
        Given expression 'poli.get(-plen) == null' evaluates to <true>
        And expression 'poli.get(plen) == null' evaluates to <true>
        And expression 'poli.get(plen*2) == null' evaluates to <true>
        Then award 10 points

    Scenario: Verifica se Poligonal.set(int,Ponto2D) esta declarado corretamente.
        Given I report 'Avaliando item 11...'
        Given I evaluate 'plen = TestUtils.random(3,10); iv = plen/2'
        Given I evaluate 'poli = new Poligonal(plen)'
        Given I get field 'vertices' value in super class of <poli> save in <pvert>
        Given I evaluate 'ptest = new PontoXZ(x1,z1)'
        Given I evaluate 'poli.set(iv,ptest)'
        And expression 'pvert[iv] == ptest' evaluates to <true>
        Then award 5 points

    Scenario: Verifica se Poligonal.set(int,Ponto2D) trata corretamente o caso i < 0 ou i >= n.
        Given I evaluate 'pvert[iv] = ptest'
        Given I evaluate 'poli.set(-plen,ptest)'
        And expression 'pvert[iv] == ptest' evaluates to <true>
        Given I evaluate 'poli.set(plen,ptest)'
        And expression 'pvert[iv] == ptest' evaluates to <true>
        Given I evaluate 'poli.set(plen*2,ptest)'
        And expression 'pvert[iv] == ptest' evaluates to <true>
        Then award 10 points

    Scenario: Verifica se Poligonal.set(int,Ponto2D) trata corretamente o caso de pontos em planos diferentes.
        Given evaluating 'poli.set(iv,new PontoYZ(y1,z1))' throws instance of 'java.lang.RuntimeException' save in <pexcept>
        And I evaluate 'emsg = pexcept.getMessage()'
        And <emsg> matches regex '^\s*[Vv][ée]rtices.+plano.*$' 
        Then award 10 points

    Scenario: Verifica se Poligonal.getComprimento() esta declarado corretamente.
        Given I report 'Avaliando item 12...'
        Given class <poliClass> declares 'getComprimento()' method save in <compMethod>
        And member <compMethod> has 'public' modifier
        And method <compMethod> returns type 'double' 
        Then award 5 points

    Scenario: Verifica se Poligonal.getComprimento() retorna o resultado correto.
        Given I evaluate 's = TestUtils.random(1.0,2.0)'
        Given I evaluate 'p0 = new PontoYZ(y1*s,z1*s); p1 = new PontoYZ(y1*s,(z1+4)*s); p2 = new PontoYZ((y1+3)*s,z1*s);'
        Given I evaluate 'poli = new Poligonal(3)'
        Given I evaluate 'poli.set(0,p0);poli.set(1,p1);poli.set(2,p2);'
        And expression 'TestUtils.equalsAprox(poli.getComprimento(),9*s,0.001)' evaluates to <true> 
        Then award 5 points

    Scenario: Verifica se a classe PoligonalFechada existe é pública e extende Poligonal.
        Given I report 'Avaliando item 13...'
        Given class 'utfpr.ct.dainf.if62c.avaliacao.PoligonalFechada' exists store class in <polifClass>
        And class <poliClass> has 'public' modifier
        And class 'utfpr.ct.dainf.if62c.avaliacao.Poligonal' is assignable from <polifClass> 
        Then award 10 points

    Scenario: Verifica se a classe PoligonalFechada declara o construtor PoligonalFechada(int) corretamente.
        Given class <polifClass> declares 'PoligonalFechada(int)' constructor save in <polifConstructor> 
        Given member <polifConstructor> has 'public' modifier
        Given I evaluate 'plen = TestUtils.random(3,10)'
        Given I evaluate 'poli = new PoligonalFechada(plen)'
        And I get field 'vertices' value in super class of <poli> save in <pvert>
        And expression 'pvert.length' evaluates to <plen>
        Then award 10 points

    Scenario: Verifica se PoligonalFechada.getComprimento() esta declarado corretamente.
        Given class <poliClass> declares 'getComprimento()' method save in <compMethod>
        And member <compMethod> has 'public' modifier
        And method <compMethod> returns type 'double' 
        Then award 5 points

    Scenario: Verifica se PoligonalFechada.getComprimento() retorna o resultado correto.
        Given I evaluate 's = TestUtils.random(1.0,2.0)'
        Given I evaluate 'p0 = new PontoYZ(y1*s,z1*s); p1 = new PontoYZ(y1*s,(z1+4)*s); p2 = new PontoYZ((y1+3)*s,z1*s);'
        Given I evaluate 'poli = new PoligonalFechada(3)'
        Given I evaluate 'poli.set(0,p0);poli.set(1,p1);poli.set(2,p2);'
        And expression 'TestUtils.equalsAprox(poli.getComprimento(),12*s,0.001)' evaluates to <true> 
        Then award 10 points

    Scenario: Verifica se a saída do programa tem os valores esperados
        Given I report 'Avaliando item 14...'
        Given I set output to <testOut>
        And I evaluate 'Avaliacao2.main(new String[0])'
        And I set output to <default>
        Given <testOut> matches regex '^.*[Cc]omprimento.+12.*$' with 'Pattern.DOTALL' option
        Then award 5 points

    Scenario: Report final grade.
        Given I report grade formatted as 'FINAL GRADE: %.1f'
