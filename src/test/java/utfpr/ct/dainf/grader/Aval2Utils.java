/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.grader;

/**
 *
 * @author Wilson
 */
public class Aval2Utils {

    public static String getPontoRegex(double x, double y, double z) {
        String xre = String.format("%f", x).substring(0,6).replace(".", "[.,]");
        String yre = String.format("%f", y).substring(0,6).replace(".", "[.,]");
        String zre = String.format("%f", z).substring(0,6).replace(".", "[.,]");
        return "^.*" + "Ponto" + "\\s*\\(\\s*" + xre + "\\d*,\\s*" + yre
                + "\\d*\\s*," + zre + "\\d*\\s*\\)\\.*$";
    }
    
    public static String getPonto2DRegex(String name, double u, double v) {
        String ure = String.format("%f", u).substring(0,6).replace(".", "[.,]");
        String vre = String.format("%f", v).substring(0,6).replace(".", "[.,]");
        return "^.*" + name + "\\s*\\(\\s*" + ure + "\\d*,\\s*" + vre
                + "\\d*\\s*\\)\\.*$";
    }

    public static double dist(double x1, double y1, double z1, double x2, double y2, double z2) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2) + Math.pow(z2-z1,2));
    }

}
