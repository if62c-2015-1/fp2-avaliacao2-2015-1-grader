
import utfpr.ct.dainf.if62c.avaliacao.Poligonal;
import utfpr.ct.dainf.if62c.avaliacao.PoligonalFechada;
import utfpr.ct.dainf.if62c.avaliacao.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Avaliacao2 {

    public static void main(String[] args) {
        Poligonal poli = new PoligonalFechada(3);
        poli.set(0, new PontoXZ(-3, 2));
        poli.set(1, new PontoXZ(-3, 6));
        poli.set(2, new PontoXZ(0, 2));
        double comp = poli.getComprimento();
        System.out.printf("Comprimento da poligonal = %f%n", comp);
    }
    
}
