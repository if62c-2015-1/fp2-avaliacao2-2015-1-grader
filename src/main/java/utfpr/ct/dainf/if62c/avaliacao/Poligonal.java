package utfpr.ct.dainf.if62c.avaliacao;

import java.util.Iterator;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Segunda avaliação parcial 2014/2.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Poligonal {
    private final Ponto2D[] vertices;
    private Ponto2D plano;

    public Poligonal(int n) {
        if (n < 2)
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        vertices = new Ponto2D[n];
    }
    
    public int getN() {
        return vertices.length;
    }
    
    public Ponto2D get(int i) {
        Ponto2D p = null;
        if (i >= 0 && i < vertices.length)
            p = vertices[i];
        return p;
    }
    
    public void set(int i, Ponto2D p) {
        if (plano == null) plano = p;
        if (plano.getClass() != p.getClass())
            throw new RuntimeException("Vértices devem estar no mesmo plano");
        if (i >= 0 && i < vertices.length)
            vertices[i] = p;
    }
    
    public double getComprimento() {
        double c = 0;
        for (int i = 0; i < vertices.length-1; i++) {
            c += vertices[i].dist(vertices[i+1]);
        }
        return c;
    }

}
