package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Segunda avaliação parcial 2014/2.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class PoligonalFechada extends Poligonal {

    public PoligonalFechada(int n) {
        super(n);
    }

    @Override
    public double getComprimento() {
        return super.getComprimento() + get(getN()-1).dist(get(0));
    }
    
}
