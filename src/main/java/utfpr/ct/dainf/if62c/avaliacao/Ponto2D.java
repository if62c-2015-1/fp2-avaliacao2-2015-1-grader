package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Segunda avaliação parcial 2014/2.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public abstract class Ponto2D extends Ponto {

    protected Ponto2D() {
    }

    protected Ponto2D(double x, double y, double z) {
        super(x, y, z);
    }
    
}
